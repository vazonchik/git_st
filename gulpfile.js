const gulp = require ("gulp");
const sass = require ("gulp-sass");
const browserSync = require('browser-sync').create();
const del = require('del');
const autoprefixer = require('gulp-autoprefixer');
const concat = require('gulp-concat');
const cleanCSS = require('gulp-clean-css');
const uglify = require('gulp-uglify');
const imagemin = require('gulp-imagemin');
const  watch = require('gulp-watch');


gulp.task('del', function () {
    return del(['dist/*'])
});

gulp.task('style', function () {
    return gulp.src('./src/sass/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 50 versions'],
            cascade: false
        }))
        .pipe(concat('all.css'))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(gulp.dest('./dist/css'));
});

gulp.task('script', function () {
    return gulp.src('./src/js/**/*.js')
        .pipe(concat('all.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./dist/js'));
})

gulp.task('copy-fonts', function () {
    return gulp.src('./src/fonts/**/*.**')
        .pipe(gulp.dest('./dist/fonts'));
})

gulp.task('copy-img', function () {
    return gulp.src('./src/img/**/*.**')
        .pipe(imagemin())
        .pipe(gulp.dest('./dist/img'));
})

gulp.task('browser-sync', function() {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });
    gulp.watch('./*.html').on('change',browserSync.reload);
    gulp.watch('./src/**/*.*', gulp.parallel('style', 'script')).on('change',browserSync.reload);
});




gulp.task('build', gulp.series('del', gulp.parallel('style', 'script', 'copy-fonts', 'copy-img')));

gulp.task('dev', gulp.series('build', 'browser-sync'));